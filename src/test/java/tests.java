
import Web.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class tests{
    WebHook webHook=new WebHook();
    MainPage mainPage=new MainPage();
    BookStore bookStore=new BookStore();
    LoginPage loginPage=new LoginPage();
    Profil profil=new Profil();
    @Test
    public void login(){
        webHook.setUp();
        mainPage.goToBookStorePage();
        bookStore.goToLoginPage();
        loginPage.login("erkansoylu","Mavi2020*");
        webHook.endOfTest();
    }

    @Test
    public void logOut(){
        webHook.setUp();
        mainPage.goToBookStorePage();
        bookStore.goToLoginPage();
        loginPage.login("erkansoylu","Mavi2020*");
        profil.logOut();
        webHook.endOfTest();

    }
    @Test
    public void addAnBookToStore(){
        String bookName="Git Pocket Guide";
        webHook.setUp();
        mainPage.goToBookStorePage();
        bookStore.goToLoginPage();
        loginPage.login("erkansoylu","Mavi2020*");
        profil.goToProfilPage();
        profil.deleteAllBooks();
        profil.goToBookStore();
        bookStore.clickToBookName(bookName);
        bookStore.addBookToCollection();
        profil.goToProfilPage();
        profil.checkToBookName(bookName);
        webHook.endOfTest();
    }

}