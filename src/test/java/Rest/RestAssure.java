package Rest;

import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static io.restassured.RestAssured.given;

public class RestAssure {

    private Logger logger = LoggerFactory.getLogger(getClass());


    public Response sendThePost(String post, String link){
        logger.info("Post is sending... ");
        logger.info("\n************************  Post  ************************");

        Response r=given()
                .contentType("application/json").body(post)
                .log().all().when().post(link)
                .then().extract().response();

        logger.info(
                "\n+++++++++++++++++ Response +++++++++++++++++\n"+
                        "\n "+r.prettyPrint()+" \n"+
                        "Response Time= "+r.getTime()+"ms"+
                        "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n");

        return r;
    }

    public Response sendThePut(String post, String link){
        logger.info("Post is sending... ");
        logger.info("\n************************  Post  ************************");

        Response r=given()
                .contentType("application/json").body(post)
                .log().all().when().put(link)
                .then().extract().response();

        logger.info(
                "\n+++++++++++++++++ Response +++++++++++++++++\n"+
                        "\n "+r.prettyPrint()+" \n"+
                        "Response Time= "+r.getTime()+"ms"+
                        "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n");

        return r;
    }


    public Response sendFile(String link, File file){
        logger.info("File is sending... ");
        logger.info("\n************************  File  ************************");

        Response r=given()
                .multiPart("file",file,"application/json")
                .post(link).thenReturn();

        logger.info(
                "\n+++++++++++++++++ Response +++++++++++++++++\n"+
                        "\n "+r.prettyPrint()+" \n"+
                        "Response Time= "+r.getTime()+"ms"+
                        "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n");

        return r;
    }

    public Response sendDel(String link){
        logger.info("Post is sending... ");
        logger.info("\n************************  Post  ************************");

        Response r=given()
                .contentType("application/json")
                .log().all().when().delete(link)
                .then().extract().response();

        logger.info(
                "\n+++++++++++++++++ Response +++++++++++++++++\n"+
                        "\n "+r.prettyPrint()+" \n"+
                        "Response Time= "+r.getTime()+"ms"+
                        "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n");

        return r;
    }

    public Response sentTheGet(String link){
        logger.info("******** Get Gönderiliyor ********");

        Response r=given()
                .contentType("application/json").log().all().when().get(link)
                .then().extract().response();

        System.out.println(
                "\n+++++++++++++++++ Response +++++++++++++++++\n"+
                        "\n "+r.prettyPrint()+" \n"+
                        "Response Time= "+r.getTime()+"ms"+
                        "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n");

        return r;
    }

}
