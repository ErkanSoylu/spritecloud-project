package Helpers;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class TxtEditor {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public String getPost(String scenarioName,String scenarioType){
        String data="";
        String post="";
        Path filePath;
        filePath= Paths.get("./Documents/"+scenarioName+"/"+scenarioType+"/Post.txt");


        logger.info(filePath.toString()+" adresinden post alınıyor");

        try {
            File myObj = new File(filePath.toString());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                 data = myReader.nextLine();
                 post= post+data+"\n";
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            Assert.fail(filePath+" pathinde bir dosya bulunamadı "+e);
        }
        if(System.getenv("AllPost")!=null){
            post=System.getenv("AllPost");
            logger.info("AllPost değeri bulundu ve 'Post=AllPost' olarak güncellendi");
        }else{
            logger.info("AllPost değeri bulunamadı");
        }


        return post;
    }

    public String getExpected(String scenarioName,String scenarioType){
        String data="";
        String post="";
        Path filePath;
        filePath= Paths.get("./Documents/"+scenarioName+"/"+scenarioType+"/Expected.txt");

        try {
            File myObj = new File(filePath.toString());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
                post= post+data+"\n";
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            Assert.fail(filePath+" pathinde bir dosya bulunamadı"+ e);
        }

        if(System.getenv("AllExpected")!=null){
            post=System.getenv("AllExpected");
            logger.info("AllExpected değeri bulundu ve 'Expexted Response=AllExpected' olarak güncellendi");
        } else{
            logger.info("AllExpected değeri bulunamadı");
        }

        logger.info(
                "\n\n\n\n+++++++++++++++++ Expexted Response +++++++++++++++++\n"+
                        post+
                "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n\n");

        return post;
    }



    public String getCurrentExcelFliName(){
        String data="";
        String post="";
        Path filePath= Paths.get("../TestReports/currentExcelFileName.txt");

        try {
            File myObj = new File(filePath.toString());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
                post= post+data+"\n";
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            Assert.fail(filePath+" pathinde bir dosya bulunamadı "+e);
        }
        if(System.getenv("AllPost")!=null){
            post=System.getenv("AllPost");
            logger.info("AllPost değeri bulundu ve 'Post=AllPost' olarak güncellendi");
        }else{
            logger.info("AllPost değeri bulunamadı");
        }


        return post.replaceAll("\n","");
    }

    public void setCurrentExcelFliName(String name) {
        try {
            FileWriter myWriter = new FileWriter("../TestReports/currentExcelFileName.txt");
            myWriter.write(name);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args)

    {
        String data="";
        String post="";
        Path filePath= Paths.get("../currentExcelFileName.txt");

        try {
            File myObj = new File(filePath.toString());
            System.out.println(myObj.getAbsoluteFile());
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data = myReader.nextLine();
                post= post+data+"\n";
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            Assert.fail(filePath+" pathinde bir dosya bulunamadı "+e);
        }
        System.out.println(post);



    }

}
