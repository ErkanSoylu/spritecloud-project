package Web;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginPage extends Metods{
    private Logger logger = LoggerFactory.getLogger(getClass());
    private By userName=By.id("userName");
    private By password=By.id("password");
    private By loginBtn=By.id("login");
    private By submitBtn=By.id("submit");

    public void login(String userNameText,String passwordText){
        sendKeyByBy(userName,userNameText);
        sendKeyByBy(password,passwordText);
        clickElementByBy(loginBtn);
        doesElementExistOtherwiseAssert(submitBtn,"Login is unsuccesfull");
    }


}
