package Web;

import com.sun.org.apache.bcel.internal.generic.FSUB;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Profil extends Metods{

    private Logger logger = LoggerFactory.getLogger(getClass());

    private By ProfilHeader=By.xpath("//*[@class=\"main-header\" and .='Profile']");
    private By profilHamburgerBtn=By.xpath("//*[.='Profile' and @class=\"text\"]");
    private By deleteBtn=By.xpath("//*[@title=\"Delete\"]");
    private By okBtn=By.id("closeSmallModal-ok");
    private By bookStore=By.id("gotoStore");
    private By bookStoreHeader=By.xpath("//*[@class=\"main-header\" and .='Book Store']");
    private By logOut=By.id("submit");
    private By loginBtn=By.id("login");

    public void goToProfilPage(){
        clickElementByBy(profilHamburgerBtn);
        doesElementExistOtherwiseAssert(ProfilHeader,"Profil Does Not Open");
    }

    public void deleteAllBooks(){

        while (doesElementExist(deleteBtn)){
            logger.info("An Book detected. Deleting...");
            clickElementByBy(deleteBtn);
            clickElementByBy(okBtn);
            waitBySeconds(3);
            clickChromeOkBtn();
            logger.info("book is deleted");
        }

    }

    public void goToBookStore(){
        clickElementByBy(bookStore);
        doesElementExistOtherwiseAssert(bookStoreHeader,"Book Store Does Not Open");
    }

    public void checkToBookName(String bookName){
        logger.info(bookName+" kitabının sayfasına gidiliyor...");
        String xpath="//*[@class=\"action-buttons\"]//span[.='"+bookName+"']";
        clickElementByBy(By.xpath(xpath));
        doesElementExistOtherwiseAssert(By.xpath("//*[.='"+bookName+"']"),bookName+" book not found");
    }

    public void logOut(){
        logger.info("logginOut");
        clickElementByBy(logOut);
        waitBySeconds(2);
        doesElementExistOtherwiseAssert(loginBtn,"loging out is unseccesful");
    }
}
