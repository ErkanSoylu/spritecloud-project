package Web;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookStore extends Metods{

    private Logger logger = LoggerFactory.getLogger(getClass());

    private By loginBtn=By.id("login");
    private By loginHuk=By.id("userForm");
    private By bookStoreHambureger=By.xpath("//*[@class=\"menu-list\"]//*[.='Book Store' and @class=\"btn btn-light active\"]");
    private By addToYourCollectionBtn=By.xpath("//div[.='Add To Your Collection']");
    private By bookStoreHeader=By.xpath("//*[@class=\"main-header\" and .='Book Store']");

    public void goToLoginPage(){
        logger.info("Going to the login page...");
        clickElementByBy(loginBtn);
        doesElementExistOtherwiseAssert(loginHuk,"Login page does not open!!!");
    }

    public void goToBookStore(){
        clickElementByBy(bookStoreHambureger);
        doesElementExistOtherwiseAssert(bookStoreHeader,"Book Store Does Not Open");
    }

    public void clickToBookName(String bookName){
        logger.info(bookName+" kitabının sayfasına gidiliyor...");
        String xpath="//*[@class=\"action-buttons\"]//span[.='"+bookName+"']";
        clickElementByBy(By.xpath(xpath));
        doesElementExistOtherwiseAssert(By.xpath("//*[.='"+bookName+"']"),bookName+" sayfasına girilemedi!!!");
    }
    public void addBookToCollection(){
        waitBySeconds(2);
        clickElementByBy(addToYourCollectionBtn);
        waitBySeconds(3);
        clickChromeOkBtn();
    }

}
