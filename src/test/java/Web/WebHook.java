package Web;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class WebHook {
    public static WebDriver driver;
    private String url="https://demoqa.com/";
    private Logger logger = LoggerFactory.getLogger(getClass());

    public void setUp(){
        logger.info("********************* Test Starting *********************");

        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

        logger.info(url+" openig...");

        driver.get(url);

        logger.info(url+" opened");

        driver.manage().window().maximize();

    }

    public void endOfTest(){
        driver.quit();
    }

}
