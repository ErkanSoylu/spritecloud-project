package Web;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainPage extends Metods{
    private Logger logger = LoggerFactory.getLogger(getClass());
    private By bookStore=By.xpath("//*[@class=\"card-body\"]//*[.='Book Store Application']");
    private By elementsHeader=By.xpath("//*[@class=\"pattern-backgound playgound-header\"]//*[.=\"Book Store\"]");
    public void goToBookStorePage(){
        waitBySeconds(5);
        clickElementByBy(bookStore);
        doesElementExistOtherwiseAssert(elementsHeader,"Elements page does not open");
    }
}