package Web;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Metods extends WebHook {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public void clickElementByBy(By by){
        WebElement element=findElement(by);
        hoverElement(element);
        element.click();
        logger.info(by+" clicked");
    }

    public void sendKeyByBy(By by, String value){
        findElement(by).sendKeys(value);
        logger.info(by+" sended");
    }
    public boolean doesElementExist(By by){
        if (findElementWithOutAssert(by)==null){
            logger.info(by+" not exist");
            return false;
        }
        return true;
    }

    public void doesElementExistOtherwiseAssert(By by,String message){
        Assert.assertTrue(message,doesElementExist(by));
    }

    public void hoverElement(WebElement element) {
        Actions actions = new Actions(WebHook.driver);
        actions.moveToElement(element).build().perform();
    }
    WebElement findElement(By by) {
        WebElement webElement=null;

        try {

            WebDriverWait webDriverWait = new WebDriverWait(driver, 5);
            webElement = webDriverWait
                    .until(ExpectedConditions.presenceOfElementLocated(by));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);

            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                    webElement);
        }catch (Exception e){
            Assert.fail(by+" Element does not exist!!!");

        }
        return webElement;
    }

    public void waitBySeconds(int seconds) {
        try {
            logger.info(seconds + " saniye bekleniyor.");
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    WebElement findElementWithOutAssert(By by) {
        WebElement webElement=null;

        try {

            WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
            webElement = webDriverWait
                    .until(ExpectedConditions.presenceOfElementLocated(by));
            ((JavascriptExecutor) driver).executeScript(
                    "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                    webElement);
        }catch (Exception e){
            logger.info(by+" Element does not exist!!!");


        }
        return webElement;
    }

    public void clickChromeOkBtn(){
        driver.switchTo().alert().accept();
    }
}