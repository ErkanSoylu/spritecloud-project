package Api;
import Api.Methods;
import Exel.LocalExcel;
import Helpers.JsonEditor;
import Helpers.TxtEditor;
import Rest.RestAssure;
import com.thoughtworks.gauge.Step;
import io.restassured.response.Response;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
public class UploadImage extends Methods{

    TxtEditor txtEditor= new TxtEditor();
    RestAssure restAssure =new RestAssure();
    LocalExcel excelReport=new LocalExcel();
    JsonEditor jsonEditor=new JsonEditor();

    private boolean error=false;
    private String errorMessage=" ";
    private String actualStatusCode;
    private String testStatus;

    private Logger logger = LoggerFactory.getLogger(getClass());
    private String postForExcel="visit_id";
    private String ignoreThePath="null";
    private String link="https://petstore.swagger.io/v2/pet/123/uploadImage";
    private String scenarioName="UploadImage";

    public void UploadImage(String scenarioType, String expectedStatusCode) throws IOException,InvalidFormatException {

        //String post= txtEditor.getPost("SurveyManagement/FileUpload",scenarioType);
        String expectedResponse=txtEditor.getExpected("UplaadImage","Default");
        //Response r=restAssure.sendThePost(post,link);


        File file = new File("./Documents/UplaadImage/image.png");

        Response r = restAssure.sendFile(link,file);

        actualStatusCode=String.valueOf(r.getStatusCode());

        if(!expectedStatusCode.equals(actualStatusCode)){
            logger.info("Durum hali hazırda fail oldugu için response karşılaştırması yapılmıyor!!!");
            error=true;
            testStatus="Fail";
            errorMessage="Expected StatusCode ="+expectedStatusCode+"Actual StatusCode= "+actualStatusCode;
        } else{
            testStatus="Pass";
            errorMessage=compare(r.prettyPrint(),expectedResponse,ignoreThePath);
        }

        error=!errorMessage.replaceAll(" ","").
                replaceAll("\n","").isEmpty();

        excelReport.writeItToLocalExcel(
                scenarioName,getDate(),"post",
                expectedStatusCode,actualStatusCode,errorMessage,jsonEditor.getValueOfPath("message",
                        r.prettyPrint()),r.getTime()+"ms",error);

        if(error){
            Assert.fail("\n*****\n"+errorMessage+"\n*****");
        }

    }
}
