package Api;

import io.restassured.response.Response;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static io.restassured.RestAssured.given;

public class Methods {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private boolean error=false;


    public Response sentTheGet(String link){
        logger.info("******** Get Gönderiliyor ********");
        Response r=given().contentType("application/json").log().all().when().get(link)
                .then().extract().response();

        logger.info(
                "\n+++++++++++++++++ Response +++++++++++++++++\n"+
                        "\n "+r.prettyPrint()+" \n"+
                        "Response Time= "+r.getTime()+"ms"+
                        "\n+++++++++++++++++++++++++++++++++++++++++++++++++\n");
        return r;
    }

    protected static void modifyFile(String filePath, String oldString, String newString)
    {
        File fileToBeModified = new File(filePath);

        String oldContent = "";

        BufferedReader reader = null;

        FileWriter writer = null;

        try
        {
            reader = new BufferedReader(new FileReader(fileToBeModified));

            //Reading all the lines of input text file into oldContent

            String line = reader.readLine();

            while (line != null)
            {
                oldContent = oldContent + line + System.lineSeparator();

                line = reader.readLine();
            }

            //Replacing oldString with newString in the oldContent

            String newContent = oldContent.replaceAll(oldString, newString);

            //Rewriting the input text file with newContent

            writer = new FileWriter(fileToBeModified);

            writer.write(newContent);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                //Closing the resources

                reader.close();

                writer.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }


    public static String getRandomNumber(){
        Random rand = new Random();

        String number="";
        number += (rand.nextInt (9000) + 1000);

        return number;
    }
    public String compare(String response, String expectedResponse, String ignoreThePath) {
        String errorMessage=" ";
        String testStatus;
        logger.info("********Response ile beklenen Response Karşılaştırılıyor********");
        JSONCompareResult result =
                JSONCompare.compareJSON(expectedResponse, response, JSONCompareMode.STRICT);
        String matris[]=result.toString()
                .replaceAll(" ","")
                .replaceAll(";","")
                .split("\n");
        if(ignoreThePath != null && !ignoreThePath.isEmpty()){

            logger.info("ignoreThePath stringinde yer alan pahtlar karşılaştırılmıyor...");

            String ignore[]=ignoreThePath.split("#");

            //ignore edilecek pathler mesajdan siliniyor
            for(int i=0;i<matris.length;i++){
                for(int j=0;j<ignore.length;j++){
                    if(matris[i].contains(ignore[j])){
                        matris[i]="";
                        try{
                            matris[i+1]="";
                            matris[i+2]="";
                        }catch (Exception e){

                        }

                        i=i+2;
                        break;
                    }
                }
            }


        } else{
            logger.info("ignoreThePath Stringinde bir deger bulunamadı.");
        }
        //null ve "null" farklı oldugu için aşagıdaki işleme gerek duyuldu
        for(int i=0;i<matris.length;i++){

            if(matris[i].contains("null")&&matris[i+1].contains("null")){
                matris[i-1]="";
                matris[i]="";
                matris[i+1]="";
                i=i+2;
                break;
            }

        }
        for(int i=0;i<matris.length;i++){

            errorMessage=errorMessage+matris[i]+"\n";
        }

        logger.info("Karşılaştırma tamamlandı ve hata mesajı düzenlendi.");
        error=!errorMessage.replaceAll(" ","").
                replaceAll("\n","").isEmpty();
        if(error){
            testStatus="Fail";
        }else{
            testStatus="Pass";
        }
        return errorMessage;
    }

    public static String equalsWhat(String text,String constant){
        System.out.println(constant+" Degerini jsonda karşılıgı aranıyor...\n");
        String equals=null;
        String s=text;

        s=s.replaceAll("\\\\","");
        s=s.replaceAll("\n","");
        s=s.replaceAll(":","");
        s=s.replaceAll(",","");
        s=s.replaceAll(" ","");
        s=s.replaceAll("\"\"","\"");
        String[] words=s.split("\"");

        for(int i =0; i<words.length;i++)
            if (constant.trim().equals(words[i].trim())){
                equals=words[i+1];
            }
        if (equals.isEmpty()){
            equals="null";
        }
        System.out.println(constant+"="+equals+" olarak bulundu");
        return equals;
    }

    public String getPostVariableForExcel(String post, String postForExcel){
        String matris[]=postForExcel.split("#");
        String postToSentExcel = "\n";
        try{
            for (int i=0;i<matris.length;i++){
                postToSentExcel=postToSentExcel+" "+matris[i]+" = "+equalsWhat(post,matris[i])+"\n";
            }
        }catch (Exception e){
            Assert.fail("getPostVariableForExcel Stringinde belirlediginiz deger post içersinde bulunamadı.");
        }

        return postToSentExcel;
    }

    public String getDate(){
        logger.info("Tarih ve saat belirleniyor...");
        SimpleDateFormat bicim2=new SimpleDateFormat("dd-M-yyyy hh.mm.ss");

        Date tarihSaat=new Date();

        return String.valueOf(bicim2.format(tarihSaat)).replaceAll(" ","\n");
    }

    public String getRandomString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        logger.info("üretilen string= "+sb.toString());
        return sb.toString();
    }

    public String getRandomPhoneNumber(){
        Random rand = new Random();

        String phoneNumber="+90";
        phoneNumber=phoneNumber+"5"+(rand.nextInt (89) + 10);
        phoneNumber=phoneNumber+(rand.nextInt (900) + 100);
        phoneNumber=phoneNumber+(rand.nextInt (89) + 10)+(rand.nextInt (89) + 10);
        return phoneNumber;
    }

    public String getRandomEmailAdress(){
        return getRandomString(5)+"@"+getRandomString(5)+".com";
    }

    public static void main(String[] args) {
        Random rand = new Random();

        String phoneNumber="+90";
        phoneNumber=phoneNumber+"5"+(rand.nextInt (89) + 10);
        phoneNumber=phoneNumber+(rand.nextInt (900) + 100);
        phoneNumber=phoneNumber+(rand.nextInt (89) + 10)+(rand.nextInt (89) + 10);

        System.out.println(phoneNumber);

    }
/*
    public String getExpectedStatusCode(String expectedResponse){


        String ExpectedStatusCode;
        if(!expectedResponse.contains("{")){
            logger.info("!!!!!Expected Respose bir json değil!!! Beklenen statuscode 400 olarak alındı!!!!!");
            return "400";
        }

        ExpectedStatusCode=jsonEditor.getValueOfPath("BeklenenStatusCode",expectedResponse);

        if(ExpectedStatusCode.equals(" ")) {
            ExpectedStatusCode=jsonEditor.getValueOfPath("responseBg.status",expectedResponse);
            if(ExpectedStatusCode.contains("true")){
                ExpectedStatusCode="200";
            }else if(ExpectedStatusCode.contains("false")){
                ExpectedStatusCode="400";
            }
        }

        if(ExpectedStatusCode.equals(" ")) {
            ExpectedStatusCode=jsonEditor.getValueOfPath("status",expectedResponse);
            if(ExpectedStatusCode.contains("true")){
                ExpectedStatusCode="200";
            }else if(ExpectedStatusCode.contains("false")){
                ExpectedStatusCode="400";
            }
        }

        if(ExpectedStatusCode.equals(" ")){
            System.out.println("expected response içersinde status code verisi bulunamadı deger varsayılan 200 olarak güncellendi");
            ExpectedStatusCode="200";
        }

        if(ExpectedStatusCode.equals(" ")){
            ExpectedStatusCode=jsonEditor.getValueOfPath("header.statusCode",expectedResponse);
        }

        logger.info("Expected Status code="+ExpectedStatusCode);


        return "ExpectedStatusCode";


    }

 */

}
