package Api;

import Exel.LocalExcel;
import Helpers.JsonEditor;
import Helpers.TxtEditor;
import Rest.RestAssure;
import io.restassured.response.Response;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class UpdateAPet extends Methods {
    TxtEditor txtEditor= new TxtEditor();
    RestAssure restAssure =new RestAssure();
    LocalExcel excelReport=new LocalExcel();
    JsonEditor jsonEditor=new JsonEditor();

    private boolean error=false;
    private String errorMessage=" ";
    private String actualStatusCode;
    private String testStatus;


    private Logger logger = LoggerFactory.getLogger(getClass());
    private String postForExcel="id";
    private String ignoreThePath="id";
    private String link="https://petstore.swagger.io/v2/pet";
    private String scenarioName="updateThePet";


    public void updateThePet(String scenarioType, String expectedStatusCode,int randomId) throws IOException, GeneralSecurityException, InvalidFormatException {

        String post=txtEditor.getPost("UpdateAPet",scenarioType).replaceAll("idHere",String.valueOf(randomId));
        String expectedResponse=txtEditor.getExpected("UpdateAPet",scenarioType).replaceAll("idHere",String.valueOf(randomId));

        Response r=restAssure.sendThePut(post,link);

        actualStatusCode=String.valueOf(r.getStatusCode());

        if(!expectedStatusCode.equals(actualStatusCode)){
            logger.info("Durum hali hazırda fail oldugu için response karşılaştırması yapılmıyor!!!");
            error=true;testStatus="Fail";
            errorMessage="Expected StatusCode ="+expectedStatusCode+"Actual StatusCode= "+actualStatusCode;
        } else{
            testStatus="Pass";
            errorMessage=compare(r.prettyPrint(),expectedResponse,ignoreThePath);
        }

        error=!errorMessage.replaceAll(" ","").
                replaceAll("\n","").isEmpty();


        excelReport.writeItToLocalExcel(
                scenarioName,getDate(),getPostVariableForExcel(r.prettyPrint(),postForExcel),
                expectedStatusCode,actualStatusCode,errorMessage,jsonEditor.getValueOfPath("message",r.prettyPrint()),r.getTime()+"ms",error);

        if(error){
            Assert.fail("\n*****\n"+errorMessage+"\n*****");
        }
    }
}
