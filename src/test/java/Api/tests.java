package Api;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class tests  {


    @Test
    public void createNetPetAndCheckIt() throws InvalidFormatException, GeneralSecurityException, IOException {
        PetNew petNew=new PetNew();
        FindsPetsById findsPetsById=new FindsPetsById();
        int randomId=petNew.getRandomId();

        petNew.petNew("Default","200",randomId);
        findsPetsById.waitBySeconds(5);
        findsPetsById.findsPetsById("1","200",randomId);
    }

    @Test
    public void createAndDeleteApath() throws InvalidFormatException, GeneralSecurityException, IOException {
        PetNew petNew=new PetNew();
        DeletePath deletePath=new DeletePath();
        FindsPetsById findsPetsById=new FindsPetsById();
        int randomId=petNew.getRandomId();

        petNew.petNew("Default","200",randomId);
        findsPetsById.waitBySeconds(5);
        deletePath.deletePathById("1","200",randomId);
    }



    @Test
    public void createAndUpdateAPet() throws IOException, InvalidFormatException, GeneralSecurityException {
        PetNew petNew=new PetNew();
        FindsPetsById findsPetsById=new FindsPetsById();
        UpdateAPet updateAPet=new UpdateAPet();
        int randomId=petNew.getRandomId();

        petNew.petNew("Default","200",randomId);
        findsPetsById.waitBySeconds(5);
        updateAPet.updateThePet("Default","200",randomId);

    }
}
