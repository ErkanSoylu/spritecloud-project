package Api;

import Exel.LocalExcel;
import Helpers.JsonEditor;
import Helpers.TxtEditor;
import Rest.RestAssure;
import io.restassured.response.Response;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DeletePath extends Methods {
    TxtEditor txtEditor= new TxtEditor();
    RestAssure restAssure =new RestAssure();
    LocalExcel excelReport=new LocalExcel();
    JsonEditor jsonEditor=new JsonEditor();
    private String scenarioName="FindsPetsById";
    private boolean error=false;
    private String errorMessage=" ";
    private String actualStatusCode;
    private String testStatus;

    private Logger logger = LoggerFactory.getLogger(getClass());
    private String ignoreThePath=null;
    private String link="https://petstore.swagger.io/v2/pet/idHere";

    public void deletePathById(String scenarioType, String expectedStatusCode, int randomId) throws IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {

        String expectedResponse=txtEditor.getExpected("DeletePath",scenarioType);
        expectedResponse=expectedResponse.replaceAll("messageHere",String.valueOf(randomId));
        link=link.replaceAll("idHere",String.valueOf(randomId));

        Response r=restAssure.sendDel(link);

        actualStatusCode=String.valueOf(r.getStatusCode());

        if(!expectedStatusCode.equals(actualStatusCode)){
            logger.info("Durum hali hazırda fail oldugu için response karşılaştırması yapılmıyor!!!");
            error=true;testStatus="Fail";
            errorMessage="Expected StatusCode ="+expectedStatusCode+"Actual StatusCode= "+actualStatusCode;
        } else{
            testStatus="Pass";
            errorMessage=compare(r.prettyPrint(),expectedResponse,ignoreThePath);
        }
        error=!errorMessage.replaceAll(" ","").
                replaceAll("\n","").isEmpty();


        excelReport.writeItToLocalExcel(
                scenarioName,getDate(),"GET",
                expectedStatusCode,actualStatusCode,errorMessage,jsonEditor.getValueOfPath("message",r.prettyPrint()),r.getTime()+"ms",error);


        if(error){
            Assert.fail("\n*****\n"+errorMessage+"\n*****");
        }

    }

    public void waitBySeconds(int seconds) {
        try {
            logger.info(seconds + " saniye bekleniyor.");
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
