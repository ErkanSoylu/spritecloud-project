package Exel;

public class contact {

    public String Scenario;
    public String Date;
    public String Post;
    public String ExpectedStatus;
    public String ActualStatus;
    public String ErrorMessage;
    public String MessageFromServis;
    public String ResponseTime;
    public String Status;

    public contact(String Scenario, String Date, String Post, String ExpectedStatus, String ActualStatus, String ErrorMessage, String MessageFromServis, String ResponseTime, String Status) {
        this.Scenario = Scenario;
        this.Date = Date;
        this.Post = Post;
        this.ExpectedStatus = ExpectedStatus;
        this.ActualStatus = ActualStatus;
        this.ErrorMessage = ErrorMessage;
        this.MessageFromServis = MessageFromServis;
        this.ResponseTime = ResponseTime;
        this.Status = Status;


    }

}