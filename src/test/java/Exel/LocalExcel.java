
package Exel;

import Helpers.TxtEditor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LocalExcel {
    TxtEditor txtEditor= new TxtEditor();

    private static String[] columns = { "Senaryo ismi", "Tarih", "Post", "Beklenen Status","Okunan Status","Hata Mesajı","Servis Mesajı","Yanıt Süresi","Durum" };
    private static List<contact> contacts = new ArrayList<contact>();

    public void writeItToLocalExcel(String Scenario, String Date, String Post, String ExpectedStatus, String ActualStatus, String ErrorMessage, String MessageFromServis, String ResponseTime, boolean error) throws IOException,
            InvalidFormatException {


        String status;
        if (error){
            status="FAIL";
        }else {
            status="PASS";
        }

        if(ErrorMessage.length()>560){
            ErrorMessage="Hata Mesajı Çok uzun, Lütfen logu inceleyiniz.";
        }
        contacts.add(new contact(Scenario,Date,Post,ExpectedStatus,ActualStatus,ErrorMessage,MessageFromServis,ResponseTime,status));
        String filename= txtEditor.getCurrentExcelFliName();

        File excelFile = new File("../TestReports/"+filename.trim().replaceAll("\n","")+".xlsx");
        FileInputStream fis = new FileInputStream(excelFile);
        Workbook workbook = new XSSFWorkbook(fis);
        Sheet sheet = workbook.getSheet("Report");

        //Sheet sheet = workbook.getSheet("test");
        System.out.println(sheet.getLastRowNum());

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

      //   Create a Row
        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
        }

        // Create Other rows and cells with contacts data
        for (contact contact : contacts) {
            Row row = sheet.createRow(sheet.getLastRowNum()+1);
            row.createCell(0).setCellValue(contact.Scenario);
            row.createCell(1).setCellValue(contact.Date);
            row.createCell(2).setCellValue(contact.Post);
            row.createCell(3).setCellValue(contact.ExpectedStatus);
            row.createCell(4).setCellValue(contact.ActualStatus);
            row.createCell(5).setCellValue(contact.ErrorMessage);
            row.createCell(6).setCellValue(contact.MessageFromServis);
            row.createCell(7).setCellValue(contact.ResponseTime);
            row.createCell(8).setCellValue(contact.Status);

        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream("../TestReports/"+filename.trim().replaceAll("\n","")+".xlsx");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void createAnExcelFile() throws IOException {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH.mm");
        Date date = new Date();
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Report");
        FileOutputStream fileOut = new FileOutputStream("../TestReports/"+formatter.format(date)+".xlsx");
        workbook.write(fileOut);
        fileOut.close();
        System.out.println("Yeni Excel Dosyasının ismi= "+formatter.format(date));
        txtEditor.setCurrentExcelFliName(formatter.format(date));

    }


}