# spritecloud-project

Hello everyone,

Test sites used:
- https://demoqa.com/ (UI) 
- https://petstore.swagger.io/ (API)

- Language : Java8(1.8)

Test Tools:
- Maven
- JUnit
- Selenium

CI/CD: Gitlab

For Web UI task I used bonigarcia's webdriver as a dependency.
Create a 3 task under tests.java.

- Test's include user login.
- Add a book to user's basket and remove the complete book lists.
- After Logout.

Which part i should do better ? 
Can check all requirements on website but have to look it as a client. Can autofill Student Registration Form too.

For API side , first I have to identify the URL's as a string value.
for this side I select 3 scenario. 

- First one is Create a new pet and check it is it on the list ?
- Second one is delete the pet and check the list is it deleted?
- Third one is update the pet list and check the list is it updated and see the new result ?


Every step I made It's keep on the record.
After API side is complete. I create a one excel file that shows every record automation made.
You can find it under TestReports file.

For Gitlab : I create a .gitlab-ci.yml file and it's include markhobson's docker image.

https://hub.docker.com/r/markhobson/maven-chrome

I could use SureFire for logging, but I think my way more easier to create a file from scratch and log it. :)



